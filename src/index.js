import React from 'react'; 
import ReactDOM from 'react-dom';

ReactDOM.render(<div><h1>Plato de la semana</h1>
    <h3>berenjenas fritas</h3>
    <p>Comensales: 4 personas</p>
    <p>Tiempo de reparación: 10 minutos</p>
    <p>Tiempo de cocción: 12 minutos</p>
    <p>Ingredientes <br></br><br></br> 
    4 berenjenas<br></br> 
    Sal <br></br> 
    Pimienta<br></br>
    4 cucharadas de haria y de aceite</p>
    <p>Preparación</p>
    <p> Lavar berenjenas.<br></br>
    Cortarlas en rodajas.<br></br>
    Espolvorearlas con sal.<br></br>
    Dejar que sulten agua por 30 minutos.<br></br>
    Enharizarlas, pomerlas a freir durante 5 minutos en aceite bien caliente.<br></br></p>
    </div>, document.getElementById("root"))
